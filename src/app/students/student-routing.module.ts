import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from '../students/view/students.view.component';
import { StudentsAddComponent } from '../students/add/students.add.component';
import { StudentsComponent } from '../students/list/students.component';

const StudentRoutes: Routes = [
  { path: 'view', component: StudentsViewComponent },
  { path: 'add' , component: StudentsAddComponent },
  { path: 'list' , component: StudentsComponent },
  { path: 'detail/:id', component: StudentsViewComponent}
];
@NgModule ({
  imports: [
    RouterModule.forRoot(StudentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class StudentRoutingModule {

}
